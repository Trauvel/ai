$('.slider__content').slick({
  infinite: false,
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 1600,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1
      },
    }
  ]
});
