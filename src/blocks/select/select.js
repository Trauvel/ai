if ($('.select__input').length) {

  $('.select__input').each(function () {

    var container = $(this).closest('.select');

    $(this).select2({
      minimumResultsForSearch: -1,
      width: '100%',
      dropdownParent: container
    });
  })
};



$('.select__input').each(function () {

  $(this).on('change', function () {
    let select = $(this)
    if ($(this).val().length > 1) {

      $(this).next('span.select2').find('ul').html(function () {
        let count = select.select2('data').length-1;
        return "<li>Выбрано " + count + "</li>";
      })
    }
  });
});