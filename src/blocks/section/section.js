if ($(window).width() > 1199) {

  var highestBox = 0;

  $('.section').each(function () {

    if ($(this).height() > highestBox) {
      highestBox = $(this).height();
      console.log(highestBox);
    }

  });
  $('.section').height(highestBox);
}



  
ScrollReveal().reveal('.reveal', {
  distance: '100px',
  duration: '500',
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  opacity: 0,
  origin: 'bottom',
  viewFactor: 1,
  delay: 200,
  interval: '50',
  scale: '1',
  mobile: false,
  reset: false
});
