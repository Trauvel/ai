if ($(window).width() >= 1200) {

  const magic = new ScrollMagic.Controller();

  const magicLength = $('.main__section').length;
  const magicDuration = `${(magicLength * 100).toString()}%`;

  new ScrollMagic.Scene({
    triggerElement: '.main__magic',
    triggerHook: '0',
    reverse: true,
    duration: magicDuration,
  }).on('progress', (event) => {
    if (event.scrollDirection === 'FORWARD') {
      for (let i = magicLength; i > 0; i--) {
        const sectionStep = 1 / magicLength;
        if (event.progress > sectionStep * i && event.progress <= sectionStep * (i + 1)) {
          $('.main__section').removeClass('main__section--active');
          $('.main__section').eq(i).addClass('main__section--active');
        };
        
        if (event.progress > 0.375 * i && event.progress <= 0.5) {
          $('.feature__animation-item').addClass('deanimate');
        }
      }
    } else if (event.scrollDirection === 'REVERSE') {
      for (let i = magicLength - 1; i >= 0; i--) {
        const sectionStep = 1 / magicLength;
        if (event.progress > sectionStep * i && event.progress <= sectionStep * (i + 1)) {
          $('.main__section').removeClass('main__section--active');
          $('.main__section').eq(i).addClass('main__section--active');
        };
        
        if (event.progress > 0.375 * i && event.progress <= 0.5) {
          $('.feature__animation-item').removeClass('deanimate');
        }
      }
    }
  }).setPin('.main__magic').addTo(magic);

}
