$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('.menu__content').toggleClass('menu__content--active');
  $('body').toggleClass('fixed');
});

if ($(window).width() < 1200) {
  $('.menu__link--header').click(function(e) {
    e.preventDefault();
    $('.menu__submenu').hide(); $(this).closest('.menu__item').find('.menu__submenu').fadeToggle();
  })
}